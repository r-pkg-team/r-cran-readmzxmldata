r-cran-readmzxmldata (2.8.3-2) UNRELEASED; urgency=medium

  * Replace Filippo by myself as Uploader (Thank you Filippo for your
    work on this package)

 -- Andreas Tille <tille@debian.org>  Thu, 09 Jan 2025 07:40:51 +0100

r-cran-readmzxmldata (2.8.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Disable reprotest
  * Standards-Version: 4.6.2 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Tue, 12 Sep 2023 02:43:03 +0200

r-cran-readmzxmldata (2.8.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 11 Nov 2022 12:39:07 +0100

r-cran-readmzxmldata (2.8.1-4) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit,
    Repository, Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sat, 16 May 2020 22:12:55 +0200

r-cran-readmzxmldata (2.8.1-3) unstable; urgency=medium

  * Team upload.
  * Switch Architecture to all from any.
  * Standards-Version: 4.2.1.
  * Remove unnecessary get-orig-source target.

 -- Dylan Aïssi <daissi@debian.org>  Tue, 02 Oct 2018 22:56:38 +0200

r-cran-readmzxmldata (2.8.1-2) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Thu, 21 Jun 2018 05:47:34 +0200

r-cran-readmzxmldata (2.8.1-1) unstable; urgency=low

  * Team upload

  [ Sebastian Gibb ]
  * New upstream release.
  * Replace own build script by a cdbs based one:
    - debian/control.in: removed.
    - debian/control: add cdbs to Build-Depends.
    - debian/rules: use cdbs based build script.
  * debian/copyright:
    - Change GPL-3 to GPL-3+.
    - Remove duplicated license text.
  * debian/control:
    - Set Standards-Version to 3.9.8.
    - Use secure canonical URL in Homepage.
    - Use secure https URLs in Vcs-Git and Vcs-Browser.

  [ Andreas Tille ]
  * cme fix dpkg-control
  * Convert to dh-r
  * Architecture: all
  * debhelper 10
  * d/watch: version=4
  * Update d/copyright

 -- Andreas Tille <tille@debian.org>  Mon, 12 Dec 2016 15:58:39 +0100

r-cran-readmzxmldata (2.8-1) unstable; urgency=low

  [ The Debichem Group ]
  * New upstream release.
  * debian/control.in: set Standards-Version to 3.9.5.

  [ Filippo Rusconi ] <lopippo@debian.org>
  * Upload.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 30 Sep 2014 09:47:16 +0200

r-cran-readmzxmldata (2.7-1) unstable; urgency=low

  [ The Debichem Group ]
  * Initial release of the Debian packaging by Sebastian Gibb
    <sgibb.debian@gmail.com>. (Closes: #714884)

  [ Filippo Rusconi ]
  * Fixes to the Vcs fields of d/control to comply with the canonical
    style.

  * Upload.

 -- Filippo Rusconi <lopippo@debian.org>  Mon, 08 Jul 2013 16:06:12 +0200
